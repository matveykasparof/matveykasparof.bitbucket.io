var umiWidgetCreatePage = Class.create({

	initialize: function (params) {
		umiWidgetCreatePage._instance = this;

		//TODO: Auto include widgets' CSS

		this.module = params['module'];
		this.method = params['method'];
		this.parent_id = params['parent-id'];
		this.files = new Array();

		this.xsltURL = "/js/client/widgets/widget.xsl";
	},
	


	run: function () {
		this.dataXmlUrl = this._getDataXmlUrl();
		this._load();
	},


	_error: function(errStr) {
		alert(errStr);
	},
	
	_createWidgetWindow: function () {
		var contentLayer = document.createElement("div");
		contentLayer.className = "widgetAll";
		document.body.appendChild(contentLayer);
		this._contentLayer = contentLayer;
	},
	
	
	_sendMessage: function (message) {
		
	},
	

	_hideMessage: function () {
		
	},


	_getDataXmlUrl: function() {
		if(!this.parent_id) {
			this._error("Widget require parent-id param to be passed.");
		}
	
		if(this.module && this.method) {
			var url = "/admin/content/widget_create/" + this.parent_id + "/.xml";
			return url;
		} else {
			this._error("TODO: Make module/method auto-detection here");
		}
	},

	
	destruct: function () {
		// clean editors
		var editors = $$('textarea.wysiwyg');
		editors.each(function (editor) {
			
			//tinyMCE.execCommand('mceToggleEditor', false, editor.id);
			tinyMCE.execCommand('mceRemoveEditor', false, editor.id);
			
		});
		if(this._contentLayer) {
			$(this._contentLayer).remove();
		}
		
		var i = 0;
		for(i = 0; i < this.files.length; i++) {
			var fc = this.files[i];
			if(fc) {
				fc.destruct();
				this.files[i] = undefined;
			}
		}
	},
	
	
	_load: function() {
		this._createWidgetWindow();
		this._loadXSLTData();
	},


	
	_loadXSLTData: function () {
		var self = this;
		var successCallback = function (transport) {
			self._onXSLTDataLoaded(transport);
		};

		var failureCallback = function (transport) {
			self._error("Ajax failed. What's on?");
		};
		
		
		var ajaxRequest = new Ajax.Request(
			this.xsltURL, {method: 'get', onSuccess: successCallback, onFailure: failureCallback}
		);
	},
	
	_onXSLTDataLoaded: function (transport) {
		this.xsltDOM = transport.responseXML;
		this._loadXMLData();
	},
	
	_loadXMLData: function () {
		var self = this;
		var callback = function (transport) {
			self._onXMLDataLoaded(transport);
		}

		var failureCallback = function (transport) {
			self._error("Ajax failed. What's on?");
		};


		this._sendMessage("Loading xml from data provider: " + this.dataXmlUrl);

		var ajaxRequest = new Ajax.Request(
			this.dataXmlUrl, {method: 'get', onSuccess: callback, onFailure: failureCallback}
		);
	},
	
	_onXMLDataLoaded: function (transport) {
		this.xmlDOM = transport.responseXML;

		this._transformXSLT();
		this._hideMessage();
	},
	
	
	_transformXSLT: function () {
		if(window.ActiveXObject) {
			var resultXML = this.xmlDOM.transformNode(this.xsltDOM);
			this._contentLayer.innerHTML = resultXML;
		} else {
			var xsltProc = new XSLTProcessor();
			xsltProc.importStylesheet(this.xsltDOM);
			var resultDOM = xsltProc.transformToDocument(this.xmlDOM);
			var serializer = new XMLSerializer();		
			this._contentLayer.innerHTML = serializer.serializeToString(resultDOM);
		}

		// init editors
		var editors = $$('textarea.wysiwyg');
		editors.each(function (editor) {
			tinyMCE.execCommand('mceToggleEditor', false, editor.id);
		});
		//setAll();	//What was that?
		
		this._initFileControls();

		//this._initFirstTab();
	},
	
	_initFirstTab: function () {
		var fieldsets = $$('#widgetContentLayer fieldset');
		var currFieldset = fieldsets.first();
		
		new Effect.Appear(currFieldset, {to: 1, duration: 0});
		this.currFieldset = currFieldset;

		//TODO: Make here wysiwyg apply
//		setTimeout(function() {tinyMCE.execCommand('mceAddControl', false, 'content_26')}, 2500);
	},
	
	
	goNext: function () {
		var fieldset = this.currFieldset.next();
		
		if(fieldset) {
			$('umiWidgetCreatePage-back-button').value = 'Back';
			if(!fieldset.next()) {
				$('umiWidgetCreatePage-next-button').value = 'Create page';
			}
			
			var cl = this.currFieldset;
			var callback = function () {
				cl.style.display = "none";
			};
		
			new Effect.Appear(this.currFieldset, {to: 0, duration: 0, afterFinish: callback});
			new Effect.Appear(fieldset, {to: 0, duration: 0, delay: 0});
			this.currFieldset = fieldset;
		} else {
			this.addPage();
		}
	},
	
	goBack: function () {
		var fieldset = this.currFieldset.previous();
		if(fieldset) {
			$('umiWidgetCreatePage-next-button').value = 'Next';
			
			if(!fieldset.previous()) {
				$('umiWidgetCreatePage-back-button').value = 'Cancel';
			}
			
			var cl = this.currFieldset;
			var callback = function () {
				cl.style.display = "none";
			};

			new Effect.Appear(this.currFieldset, {to: 0, duration: 0, afterFinish: callback});
			new Effect.Appear(fieldset, {to: 0, duration: 0, delay: 0});
			this.currFieldset = fieldset;
		} else {
			this.closeWidget();
		}
	},
	
	closeWidget: function() {
		this.destruct();
	},
	
	addPage: function () {
		// clean editors
		var editors = $$('textarea.wysiwyg');
		editors.each(function (editor) {
			var inst = tinyMCE.getInstanceById(editor.id);
			if (inst) {
				inst.triggerSave();
			}
		});

		var data = $('add-page').serialize(true);
		var callback = function () {
			var s_location = window.location.href;
			var arr_l = s_location.split('#');
			var url = arr_l[0];
			if (url.indexOf('editable=1') == -1) {
				url += '?&editable=1';
			}
			window.location.href = url;
		};
		
		var url = this.dataXmlUrl + "?do=do";
		var ajaxRequest = new Ajax.Request(
			url, {method: 'post', parameters: data, onSuccess: callback}
		);
	},
	
	_initFileControls: function() {
		var nodes = this._contentLayer.getElementsBySelector('div.file');
		var i;
		
		for(i = 0; i < nodes.length; i++) {
			var node = nodes[i];

			var fc = new umiFileControl({
				'name': node.id,
				'styles': {
					'width': '200px'
				}
			});
			this.files[this.files.length] = fc;
		}
	}

});

umiWidgetCreatePage.getInstance = function () {
	return umiWidgetCreatePage._instance;
};

function callWidget() {
//	var testWidget = new umiWidgetCreatePage({module: 'content', method: 'page', 'parent-id': 23011});
//	var testWidget = new umiWidgetCreatePage({module: 'catalog', method: 'object', 'parent-id': 23011});
//	testWidget.run();
}

setTimeout(callWidget, 2000);
/*
function postWidget() {
	var data = $('add-page').serialize(true);
	var callback = function () {
		window.location.reload();
	};

	var ajaxRequest = new Ajax.Request(
		"/admin/content/add/23053/page/do.xml", {method: 'post', parameters: data, onSuccess: callback}
	);
}
*/