/**
* Сохраняем форму. Функция принимает ссылку на форму. Форма должна иметь
* уникальный аттрибут ID.
*/

function saveFormSession(form) {
	if(!form||!form.id||!/^[^;=]+$/.test(form.id)) return;

	if ( (form.email.value == '') || ( form.email.value.match(/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/))== null ) {
	alert('Введите корректный E-Mail адрес');
	form.email.focus();
	return false;
	}

if((form.login.value == '')||(form.password.value == '')||(form.password_confirm.value == '')||(form.password.value != form.password_confirm.value)||(form.gender.value == '')||(form.age.value == '')){
	if ( form.login.value == '' ) {
	alert('Введите логин');
	form.login.focus();
	return false;
	}
	
	if ( form.password.value == '' ) {
	alert('Введите пароль');
	form.password.focus();
	return false;
	}
	
	if ( form.password_confirm.value == '' ) {
	alert('Введите подтверждение пароля');
	form.password_confirm.focus();
	return false;
	}
	
	if ( form.password.value != form.password_confirm.value ) {
	alert('Введенные пароли не совпадают');
	form.password_confirm.focus();
	return false;
	}
	
	if ( form.gender.value == '' ) {
	alert('Укажите ваш пол');
	form.gender.focus();
	return false;
	}
	
	if ( form.age.value == '' ) {
	alert('Укажите ваш возраст');
	form.age.focus();
	return false;
	}
}else{
		var data="", tok, el, safe_name;
		for(var i=0; i<form.elements.length; i++) {
			if((el=form.elements[i]).name==""||el.getAttribute("skip_form_save")!=null) continue;
			safe_name=el.name.replace(/([)\\])/g, "\\$1");
			switch(el.type) {
				case "text":
				case "textarea": tok="v("+safe_name+"):"+el.value.replace(/([|\\])/g, "\\$1")+"||"; break;
				case "radio":
				case "checkbox": tok="s("+safe_name+"):"+(el.checked? "1": "0")+"||"; break;
				case "select-one": tok="i("+safe_name+"):"+(el.selectedIndex)+"||"; break;
				default: tok="";
			}
			data+=tok;
		}
		if(data>=4000) return alert("Can't save form into cookie, to much data...");
		document.cookie="sss"+form.id+"="+escape(data);
}
return true;
}


function saveFormSessionAuth(form) {
//	if(!form||!form.id||!/^[^;=]+$/.test(form.id)) return;

if((form.login.value == '')||(form.password.value == '')){
	if ( form.login.value == '' ) {
	alert('Введите логин');
	form.login.focus();
	return false;
	}
	
	if ( form.password.value == '' ) {
	alert('Введите пароль');
	form.password.focus();
	return false;
	}
}
document.cookie="writeauth=1";
return true;
}

/**
* Восстановить значение формы. Форма должна иметь уникальный аттрибут ID.
*/
function restoreFormSession(form) {
	if(!form||!form.id||!/^[^;=]+$/.test(form.id)) return false;
		var strt, end, data, nm, dat;
		if((strt=document.cookie.indexOf("sss"+form.id))<0) return false;
		if((end=document.cookie.indexOf(";", strt + form.id.length + 3))<0) end=document.cookie.length;
		data=unescape(document.cookie.substring(strt + form.id.length + 4, end)).split("||");
		for(var i=0; i<data.length-1; i++) {
			nm=/^[vsi]\(((?:[^)\\]|(?:\\\))|(?:\\\\))+)\)\:/.exec(data[i]);
			nm[1]=nm[1].replace(/\\([)\\])/g, "$1");
			dat=data[i].substr(nm[0].length).replace(/\\([|\\])/g, "$1");
			switch(data[i].charAt(0)) {
				case "v": form.elements[nm[1]].value=dat; break;
				case "s": form.elements[nm[1]].checked=(dat=="1"? true: false); break;
				case "i": form.elements[nm[1]].selectedIndex=dat; break;
			}
		}
 
	return true;
}

/*
if(window.addEventListener){ // Mozilla, Netscape, Firefox
	window.addEventListener("load",restoreFormSession(forma), false);
	alert('FF');
} else { // IE
	alert('IE');
	window.attachEvent("onload", return restoreFormSession('forma'));
}
*/

//initEvent("load", function () {alert ('Event 2');});
//window.onload = function () {alert ('Event 1');};

//document.body.onload = restoreFormSession(form);