User-agent: *
Disallow: /connectors/
Disallow: /assets/components/
Disallow: /search
Disallow: /index.php
Disallow: /?
Host: /
Sitemap: /sitemap.xml
